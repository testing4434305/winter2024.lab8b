public enum Tile{
	BLANK("_"),
	WALL("W"),
	HIDDEN_WALL("_"),
	CASTLE("C");
	//field
	private String name;
	
	//construcor
	private Tile(String name){
		this.name = name;
	}
	public String getName(){
		return this.name;
	}
}