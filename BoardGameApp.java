import java.util.Scanner;
public class BoardGameApp{
	public static void main(String[] args){
		Scanner reader = new Scanner(System.in);
		Board game = new Board();
		Tile playerToken = Tile.WALL;
		
		System.out.println("Hey! let's play the Castle Game!");
		int numCastles = 7;
		int turns = 0;
		int result = 0;
		while(numCastles>0 && turns <8){
			System.out.println(game.toString());
			System.out.println(numCastles);
			System.out.println(turns);System.out.println("Enter 2 values. First for the row, and the second is for the column.");
			int row = reader.nextInt();
			int col = reader.nextInt();
			while(result<0){
				System.out.println(turns);System.out.println("Enter other values, the output is negative!");
				row = reader.nextInt();
				col = reader.nextInt();
			}
			result = game.placeToken(row,col);
			if(result == 1){
				System.out.println("Sorry, there's a wall!");
				turns++;
			}
			else{
				System.out.println("Castle is placed!");
				numCastles--;
				turns++;
			}
		}
		
		System.out.println(game.toString());
		if(numCastles == 0){
			System.out.println("You won!");
		}
		else{
			System.out.println("You lost!");
		}
	}
}