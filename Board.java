import java.util.Random;
public class Board{
	//field
	private Tile[][] grid;
	//construcor
	public Board(){
		Random rng = new Random();
		int size = 5;
		int index = 0;
		this.grid = new Tile[size][size];
		for(int i=0; i<grid.length; i++){
			int randomCol = rng.nextInt(size);
			grid[i][randomCol] = Tile.HIDDEN_WALL;
			for(int j=0; j<grid[i].length; j++){
				if (j != randomCol) {
				grid[i][j]=Tile.BLANK;
				}
			}
		}
	}
	public String toString(){
		String boardRow = "";
		for(Tile[] arrays:this.grid){
			for(Tile value:arrays){
				boardRow+=value.getName();
				boardRow+=" ";
			}
			boardRow+="\n";
		}
		return boardRow;
	}
	public int placeToken(int row, int col){
		Tile positionCastle = Tile.CASTLE;
		Tile positionWall = Tile.WALL;
		Tile positionHiddenWall = Tile.HIDDEN_WALL;
		Tile positionBlank = Tile.BLANK;
		int arraySize = 5;
		if(row>arraySize || row<1){
			return -2;
		}
		if(col>arraySize || col<1){
			return -2;
		}
		if(this.grid[row-1][col-1] == positionCastle || this.grid[row-1][col-1] == positionWall){
			return -1;
		}
		if(this.grid[row-1][col-1] == positionHiddenWall){
			return 1;
		}
		else{
			this.grid[row-1][col-1] = Tile.CASTLE;
			return 0;
		}
	}
}